

public aspect TraceAspect_Wilson {
	
   pointcut methodToTrace(): execution(String getName());

   before(): methodToTrace() {
      System.out.println("BEFORE: " + thisJoinPointStaticPart.getSignature() +
            ", " + thisJoinPointStaticPart.getSourceLocation().getLine());
   }

   after(): methodToTrace() {
      System.out.println("AFTER: " + thisJoinPointStaticPart.getSourceLocation().getFileName());
   }
}
